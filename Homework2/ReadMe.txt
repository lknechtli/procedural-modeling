CSE 313 Homework 2:
Lucien Knechtli

Hotkeys: haven't changed them	
q = quit
r = rotation mode
t = translation mode
z = z axis rotation
x = x axis rotation
y = y axis rotation
+ = zoom in
- = zoom out

Rather than implementing shadows or antialiasing, I opted to create 3 randomly generated
trees which have semi-realistic proportions and branching patters.

I did this by semi-randomly determining the number of branches and the location/angle of
the branches at each level of recursion, resulting in a model that looks somewhat like
a natural tree in branching behavior.  Every time that program is run, the 3 trees will
look different and be placed in different locations.
Most of the changes related to this are found in TreePart.cpp, with Tree.cpp changed only
in the arguments used to begin creating the tree.

I also wrote code for the random and non-overlapping distribution of trees (separated by
at least 10 units)

My computer runs the simulation fine, but you may wish to change the number of trees if
they slow down the rendering too much ( there are many objects required to make the trees
look nice).  To do this, change numObstacles in Scene.h